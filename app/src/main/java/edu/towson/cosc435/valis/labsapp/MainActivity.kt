package edu.towson.cosc435.valis.labsapp

import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import edu.towson.cosc435.valis.labsapp.model.Album
import edu.towson.cosc435.valis.labsapp.model.Song
import edu.towson.cosc435.valis.labsapp.ui.SongRow
import edu.towson.cosc435.valis.labsapp.ui.theme.LabsAppTheme

class MainActivity : ComponentActivity() {

    @ExperimentalFoundationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            LabsAppTheme {
                Surface(color = MaterialTheme.colors.background) {
                    val originalSongs = (1..21).map { songNum ->
                        Song("Song $songNum", "Kanye", songNum, songNum % 3 == 0)
                    }
                    var songs by remember { mutableStateOf(originalSongs) }
                    MainScreen(
                        songs = songs,
                        onDelete = { deletedSong -> songs = songs.filter { song ->
                            song.name != deletedSong.name
                            //new list will not include deleted song from original song list
                        }
                        }
                    )
                }
            }
        }
    }
}

@ExperimentalFoundationApi
@Composable
fun MainScreen(
    songs: List<Song>,
    onDelete: (Song) -> Unit
) {
    Column(
    ) {
        // TODO - 3d. Add Search bar here
        LazyColumn() {
            itemsIndexed(songs) { idx, song ->
                SongRow(idx, song, onDelete)
            }
        }
    }
}

// TODO - 3. Implement the Song row checkbox to track the "IsAwesome" state
// TODO - 4. Implement a search bar for filtering the list.
// TODO - 4a. Create a new Composable (Row with OutlinedTextField and Button) for the Search feature
// TODO - 4b. Add internal mutable state to track the text input for the Search composable.
// TODO - 4c. Hoist a function that handles the search button click

@ExperimentalFoundationApi
@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    val songs = listOf(Song("Thriller", "MJ", 1, true), Song("Hotline Bling", "Drake", 2, false))
    LabsAppTheme {
        Surface(color = MaterialTheme.colors.background) {
            MainScreen(songs)
        }
    }
}