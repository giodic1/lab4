package edu.towson.cosc435.valis.labsapp.model

data class Song(
    val name: String,
    val artist: String,
    val track: Int,
    val isAwesome: Boolean
    ) {
}