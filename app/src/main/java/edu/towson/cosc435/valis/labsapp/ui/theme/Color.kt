package edu.towson.cosc435.valis.labsapp.ui.theme

import androidx.compose.ui.graphics.Color

val Teal = Color(0xFF008577)
val DarkTeal = Color(0xFF00574B)
val Pink = Color(0xFFD81B60)
val Grey = Color(0xFFEEEEEE)